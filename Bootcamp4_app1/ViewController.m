//
//  ViewController.m
//  Bootcamp4_app1
//
//  Created by Jeremi Kaczmarczyk on 16.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)buttonPress:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *progressBarr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarWidth;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUp];
}

- (void)setUp {
    [self.activityIndicator setHidden:YES];
    [self.progressBarr setHidden:YES];
}

- (IBAction)buttonPress:(id)sender{
    __weak typeof(self) weakSelf = self;

    [UIView animateWithDuration:1 animations:^{
        [weakSelf.button setAlpha:0];
    } completion:^(BOOL finished){
        [weakSelf.button setHidden:YES];
        [weakSelf.activityIndicator setHidden:NO];
        [weakSelf.activityIndicator startAnimating];
        [weakSelf.progressBarr setHidden:NO];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

        int time = arc4random_uniform(5)+1;
        [weakSelf.view layoutIfNeeded];
        [UIView animateWithDuration:time animations:^{
                [weakSelf.progressBarWidth setConstant:180];
                [weakSelf.view layoutIfNeeded];
        }completion:^(BOOL finished){
            [weakSelf.activityIndicator stopAnimating];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [weakSelf.activityIndicator setHidden:YES];
        }];
        
    }];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
